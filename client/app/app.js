/* Created by Leong Fong Sow  on 21/10/2016.
 Name: app.js
*/
(function(){
    var CAApp = angular.module("CAApp", []);

    var MyCtrl = function($http, $window) {
        var vm = this;
        vm.message = "Hello";
        vm.userObj = {};

        //Define the User object
        var defaultUserObj = {
            email: "",
            password: "",
            name: "",
            gender: "",
            dob: "",
            address: "",
            country: "",
            contact: "",
        }

        vm.userObj = angular.copy(defaultUserObj);

        vm.isAgeValid = function() {
            //console.log("the function called")
            var inputDate = new Date(vm.userObj.dob);
            inputDate.setFullYear(inputDate.getFullYear() + 18);
            //vm.$setValidity = (inputDate < new Date());
            //console.log(vm.$setValidity);
            //ctrl.$setValidity('invalidshrt',true);
            return inputDate < new Date();
        }

        vm.submit = function() {
             //console.log("User Registration Form Submitted");
             $http.post("/api/regform", vm.userObj)
                .then(function(response){
                    console.log("OK");
                    //console.log(response);
                    $window.location.href = '/regFormTK.html';

                })
                .catch(function(response) {
                    console.log("NOK");
                    //console.log(response);
                })

        }
    }

    CAApp.controller("MyCtrl", ["$http", "$window",MyCtrl])

    //The following code is yet to work
    CAApp.directive('goodAge', function() {
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.goodAge = function(modelValue, viewValue) {
                    var inputDate = new Date(modelValue);
                    inputDate.setFullYear(inputDate.getFullYear() + 18);
                    //vm.$setValidity = (inputDate < new Date());
                    //console.log(vm.$setValidity);
                    //ctrl.$setValidity('invalidshrt',true);
                    console.log("testing");
                    console.log(inputDate < new Date());
                    return (inputDate < new Date());

                };
            }
        };
    });

})();
