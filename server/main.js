/* Created by Leong Fong Sow  on 21/10/2016.
   Name: main.js
 */

/* Define the Node Modules */
const express = require("express");
const app = express();

const bodyParser = require('body-parser');

const path = require("path");
const docroot = path.join(__dirname, "..");

app.set("port", process.argv[2] || process.env.NODE_PORT || 3000);
app.use(express.static(path.join(docroot, "client")));
app.use("/bower_components",
    express.static(path.join(docroot, "bower_components")));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post("/api/regform", function(req, res, next) {
    console.log("Sever Post - DONE v2");
    console.log("Email: " + req.body.email);
    console.log("Name: " + req.body.name);
    console.log(req.body);
    res.redirect("/regFormTK.html");
})


app.listen(app.get("port"), function () {
    console.log("Server running at http://localhost: %s",
        app.get("port"));
})


